FROM debian:sid-slim                                                                                                   

RUN apt-get -y update && apt-get -y install wget unzip postgresql-13 postgis lynx xz-utils gdal-bin make time csvkit && rm -rf /var/lib/apt/lists/*

