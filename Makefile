all: download unzip pgschema load sales

clean:
	psql -c 'DROP SCHEMA psi CASCADE;'

clean_sales:
	rm -rf data

clean_sales_unzip:
	rm -rf data_sales_unzip

clean_lot_zip:
	rm -rf build/NSW_LandParcelLot.zip

clean_property_zip:
	rm -rf build/NSW_Property.geojson.gz

## DOWNLOAD

download: download_sales download_values

download_sales_ci:
	wget --no-verbose --recursive --no-parent --reject "index.html" https://tianjara.net/data/valuation-property-nsw/ && mv tianjara.net/data/valuation-property-nsw data

download_sales:
	./nsw-property-sales-mirror

download_values:
	./land-value-information-mirror --latest

unzip: unzip_sales unzip_values

unzip_sales:
	./nsw-property-sales-unzip
unzip_values:
	./land-value-information-unzip

## LOAD

pgschema: pgschema_psi pgschema_sales pgschema_values

pgschema_psi:
	psql -f schema/psi.sql
	cat schema/zones.csv | psql -c "COPY psi.zones FROM STDIN WITH DELIMITER ',';"

pgschema_sales:
	psql -f schema/sales_raw.sql
	psql -f schema/median.sql

pgschema_values:
	psql -f schema/values_raw.sql

validate_raw_data:
	#cat schema/zones.csv | psql -c "COPY psi.sales_raw_b FROM STDIN WITH DELIMITER ',';"
	./validate.sh

load: load_sales load_values

load_sales:
	time ./load_sales.sh

load_values:
	time ./load_values.sh

sales: schema/sales.sql
	time psql -f $<

values: schema/values.sql
	time psql -f $<

drop_raw: drop_raw_sales drop_raw_values

drop_raw_sales:
	psql -c 'DROP TABLE psi.sales_archived_raw;'
	psql -c 'DROP TABLE psi.sales_current_raw;'

drop_raw_values:
	psql -c 'DROP TABLE psi.values_raw;'

build/NSW_LandParcelLot.zip:
	mkdir -p build
	wget --no-verbose -O $@ https://tianjara.net/data/NSW_LandParcelLot.zip

lot: build/NSW_LandParcelLot.zip
	time ogr2ogr -f PostgreSQL PG: /vsizip/$</Lot_EPSG4283.gdb -select 'cadid' -lco SCHEMA=psi -lco UNLOGGED=YES -lco OVERWRITE=YES -nln lot -lco GEOMETRY_NAME=geom Lot
	psql -c 'CREATE INDEX ON psi.lot (cadid);'

build/NSW_Property.geojson.gz:
	mkdir -p build
	wget --no-verbose -O $@ https://tianjara.net/data/NSW_Property.geojson.gz

property: build/NSW_Property.geojson.gz
	ogr2ogr -f PostgreSQL -lco SCHEMA=psi -lco UNLOGGED=YES -lco OVERWRITE=YES -nln property PG: /vsigzip/$<
	psql -c 'CREATE INDEX ON psi.property (propid);'
	psql -c 'CREATE INDEX ON psi.property (cadid);'

geometry: sales_geometry values_geometry

sales_geometry: schema/sales_geometry.sql
	time psql -f $<

values_geometry: schema/values_geometry.sql
	time psql -f $<

sales_extra: sql/sale_profit.sql sql/sale_hold_time.sql
	time psql -f sql/sale_profit.sql
	time psql -f sql/sale_hold_time.sql

## OUTPUT

log_size:
	psql -c "SELECT table_name, pg_relation_size(quote_ident(table_name)) FROM information_schema.tables WHERE table_schema = 'psi' order by 2;"

dump:
	mkdir -p output
	pg_dump --schema=psi --no-owner | gzip --stdout > output/nsw-property-sales.sql.gz

# if you only care about the _geometry tables then all these can be dropped
drop_sales:
	psql -c 'DROP TABLE psi.sales;'

drop_values:
	psql -c 'DROP TABLE psi.values;'

drop_property:
	psql -c 'DROP TABLE psi.property;'

csv: csv_sales csv_values

csv_sales:
	./csv_sales.sh

csv_values:
	./csv_values.sh

mbtiles:
	time ./mbtiles.sh

sales_geometry_geojson: schema/sales_geometry_geojson.sql
	psql -f $<

values_geometry_geojson: schema/values_geometry_geojson.sql
	psql -f $<

sales_geojson:
	mkdir -p output
	time ogr2ogr -f GeoJSONSeq -t_srs 'EPSG:4326' -lco COORDINATE_PRECISION=5 /vsigzip/output/nsw-property-sales.geojson.gz PG: psi.sales_geometry_geojson

sales_flatgeobuf:
	mkdir -p output
	time ogr2ogr -f FlatGeobuf -t_srs 'EPSG:4326' output/nsw-property-sales.fgb PG: psi.sales_geometry_geojson
	gzip output/nsw-property-sales.fgb

values_geojson:
	mkdir -p output
	time ogr2ogr -f GeoJSONSeq -t_srs 'EPSG:4326' -lco COORDINATE_PRECISION=5 /vsigzip/output/nsw-land-values.geojson.gz PG: psi.values_geometry_geojson

values_flatgeobuf:
	mkdir -p output
	time ogr2ogr -f FlatGeobuf -t_srs 'EPSG:4326' output/nsw-land-values.fgb PG: psi.values_geometry_geojson
	gzip output/nsw-land-values.fgb

SuburbsLocalities.tar.xz:
	wget --no-verbose -O $@ 'https://tianjara.net/data/psma-admin-bdys/Suburbs%20-%20Localities%20AUGUST%202020%20-%20Copy.tar.xz'

SuburbsLocalities: SuburbsLocalities.tar.xz
	tar -xvvJf $<

loadSuburbsLocalities:
	ogr2ogr -lco OVERWRITE=yes -lco GEOMETRY_NAME=geom -lco SCHEMA=psi -lco UNLOGGED=YES -t_srs 'EPSG:4326' PG: Suburbs\ -\ Localities\ AUGUST\ 2020\ -\ Copy.shp -nln suburbs_localities -nlt MULTIPOLYGON -where "state_pid = '1'"

build/sua.zip:
	mkdir -p build
	wget --no-verbose -O $@ 'https://www.abs.gov.au/ausstats/subscriber.nsf/log?openagent&1270055004_sua_2016_aust_shape.zip&1270.0.55.004&Data%20Cubes&1E24D1FB300696D2CA2581B1000E15A5&0&July%202016&09.10.2017&Latest'

build/sa1.zip:
	mkdir -p build
	wget --no-verbose -O $@ 'https://www.abs.gov.au/AUSSTATS/subscriber.nsf/log?openagent&1270055001_sa1_2016_aust_shape.zip&1270.0.55.001&Data%20Cubes&6F308688D810CEF3CA257FED0013C62D&0&July%202016&12.07.2016&Latest'

loadSUA: build/sua.zip
	ogr2ogr -f PostgreSQL PG: /vsizip/$</SUA_2016_AUST.shp  -lco UNLOGGED=YES -lco SCHEMA=psi -lco GEOMETRY_NAME=geom -nln sua -t_srs 'EPSG:4326' -nlt MULTIPOLYGON  -where "SUA_CODE16 LIKE '1%' AND SUA_CODE16 != '1000'" -select 'sua_name16'

loadSA1: build/sa1.zip
	ogr2ogr -f PostgreSQL PG: /vsizip/$</SA1_2016_AUST.shp  -lco UNLOGGED=YES -lco SCHEMA=psi -lco GEOMETRY_NAME=geom -nln sa1 -t_srs 'EPSG:4326' -nlt MULTIPOLYGON  -where "STE_CODE16 = '1'" -select ''

loadMonthlyCityMedians: queries/monthly_city_medians.sql
	psql -f $<

loadSA1Medians: queries/sa1_median.sql
	psql -f $<

output/monthly_city_medians.json: exports/monthly_city_medians.sql
	mkdir -p export
	psql --tuples-only --no-align -f $< > $@
