# nsw-property-sales-scripts

UNIX scripts to work with [NSW Property Sales data](https://valuation.property.nsw.gov.au/embed/propertySalesInformation) and [NSW land value information](https://www.valuergeneral.nsw.gov.au/land_value_summaries/lv.php).

![NSW Land Values visualisation](img/nsw-land-values.png)

The source data is released by Property NSW under the Creative Commons Attribution 4.0 license.

When running the first time

- Download source data with `make download`
  `build/processed.txt` will be created with a list of source URL files downloaded. Next time the download is run only new files will be downloaded.
- Extract with `make unzip`
- Load into PostgreSQL with `make pgschema load`, `make -j sales values`
- If not needing incremental updates then, drop intermediate tables `make drop_raw`
- Export to CSV with `make csv`

Subsequent runs may be done incrementally

- `make download` - download's what's new only
- `make unzip`

# Matching to NSW Property

The property sales data is aspatial (no geometries) it only contains the street address and a property id field.

Land parcel polygons can be downloaded via a [data export](https://portal.spatial.nsw.gov.au/portal/home/item.html?id=6a96afdba8d6407d8bde5a3bbacb0f02) however this doesn't contain the property id field from the property sales data.

The spatial web service for [PropertyLot](https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Property/MapServer/5) contains both `propid` and `cadid` linking the two datasets together, however downloading it is intensive.

Since this web service is open licensed, a cached version should be used in most circumstances.

- `make -j lot property` will:
 - download a static NSW Property database
 - load into PostgreSQL
- Join sales with property geometry with `make geometry`
- Optionally export to GeoJSON with `make geojson`
- Optionally export to MBTiles with `make mbtiles`

## Sample Queries

A few sample queries are provided:

1. Monthly City Medians. Using the ABS SUA areas, a table is produced containing monthly medians for each significant urban area.
2. SA1 Medians. Median price over the past few years for each SA1
