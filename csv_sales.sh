#!/bin/sh

set -e

mkdir -p output
export dir=`pwd`
touch output/nsw-property-sales.csv.xz
chmod a+w output/nsw-property-sales.csv.xz
time psql -c "COPY psi.sales TO PROGRAM 'xz -T0 --stdout > ${dir}/output/nsw-property-sales.csv.xz' WITH CSV HEADER;"
