#!/bin/sh

set -e

mkdir -p output
export dir=`pwd`
touch output/nsw-property-values.csv.xz
chmod a+w output/nsw-property-values.csv.xz
time psql -c "COPY psi.values TO PROGRAM 'xz -T0 --stdout > ${dir}/output/nsw-property-values.csv.xz' WITH CSV HEADER;"
