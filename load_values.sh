#!/bin/sh

set -e

find \
    data_values_unzip/LV* \
    -type f -name '*.csv' -print0 | \
    sort --zero-terminated --numeric-sort | \
    xargs -I '{}' -0 sed '1d' '{}' | \
    iconv -f utf-8 -t utf-8 -c | \
    csvcut --encoding us-ascii --no-header-row -c 3-16 | sed '1d' | \
    psql -c "COPY psi.values_raw FROM STDIN WITH (FORMAT csv, HEADER false, DELIMITER ',', NULL '');"
