#!/bin/sh

psql -f schema/sales_geometry_mbtiles.sql

ogr2ogr \
    -f GeoJSONSeq \
    -t_srs 'EPSG:4326' \
    -lco RFC7946=YES \
    -lco COORDINATE_PRECISION=5 \
    /vsistdout \
    PG: psi.sales_geometry_mbtiles | \
tippecanoe \
    --force \
    --output output/psi.mbtiles \
    --name "NSW Property Sales Information" \
    --attribution "NSW Property Sales Information CC BY 4.0" \
    --layer psi \
    --no-feature-limit \
    --no-tile-size-limit \
    --exclude "day" \
    --drop-rate 1 \
    --minimum-zoom 8 \
    --maximum-zoom 14 \
    --base-zoom 14

    #--cluster-distance 10 \
    #--include "price" \
    #--feature-filter '{ "year": [ "in", 2018 ] }' \
    #--accumulate-attribute=price:mean \
