CREATE MATERIALIZED VIEW psi.monthly_city_medians AS
    SELECT
      date_part('year', sales.contract_date) AS year,
      date_part('month', sales.contract_date) AS month,
      round(median(sales.purchase_price)) AS median_purchase_price,
      count(*) AS count,
      strata_lot_number IS NOT NULL AS strata,
      city.sua_name16 AS name
    FROM
      psi.sales_geometry AS sales,
      psi.sua AS city
    WHERE
      ST_Within(sales.geom, city.geom)
    GROUP BY
      city.sua_name16,
      year,
      month,
      strata;
