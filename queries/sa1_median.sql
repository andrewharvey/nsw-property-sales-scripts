CREATE MATERIALIZED VIEW psi.sa1_medians AS
  SELECT
    sa1.sa1_main16 AS code,
    median(sales.purchase_price),
    sa1.geom
  FROM
    psi.sales_geometry AS sales,
    psi.sa1 AS sa1
  WHERE
    sales.contract_date > date '2017-10-01'
    AND ST_Within(sales.geom, sa1.geom)
    AND sales.strata_lot_number IS NULL
    AND sales.primary_purpose = 'RESIDENCE'
  GROUP BY
    sa1.sa1_main16,
    sa1.geom;

