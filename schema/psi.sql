DROP SCHEMA IF EXISTS psi CASCADE;
CREATE SCHEMA psi;

CREATE EXTENSION IF NOT EXISTS postgis;

CREATE DOMAIN psi.zone AS text;
CREATE UNLOGGED TABLE psi.zones
(
    "code" psi.zone PRIMARY KEY,
    "name" text,
    "class" text
);
