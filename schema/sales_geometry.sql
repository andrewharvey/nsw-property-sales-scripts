CREATE INDEX IF NOT EXISTS sales_property_id_idx ON psi.sales (property_id);

-- CREATE TABLE IF NOT EXISTS psi.sales_geometry (LIKE psi.sales INCLUDING ALL, geom geometry);

-- assign property_id to lot
DROP TABLE IF EXISTS psi.lot_property;
CREATE TABLE psi.lot_property AS (
  SELECT
    lot.*,
    property.propid
  FROM
    psi.lot lot
    LEFT JOIN
    psi.property property
    ON lot.cadid::integer = property.cadid
);
CREATE INDEX IF NOT EXISTS lot_property_propid_idx ON psi.lot_property (propid);

-- sales table + parcel geometry + centroid
DROP TABLE IF EXISTS psi.sales_geometry;
CREATE TABLE psi.sales_geometry (LIKE psi.sales INCLUDING ALL, centroid geometry, geom geometry);
INSERT INTO psi.sales_geometry
    SELECT
        --ROW_NUMBER() OVER (ORDER BY 1) AS id,
        sales.*,
        ST_Centroid(lot.geom) AS centroid,
        lot.geom AS geom
    FROM
        psi.sales sales
        LEFT JOIN
        psi.lot_property lot
        ON sales.property_id = lot.propid AND lot.propid IS NOT NULL
        ;

-- aggregate all sales into a single property feature
CREATE TABLE psi.sales_per_property AS (
  SELECT
    lot.propid,
    array_agg(sales.purchase_price) purchase_prices,
    ST_Centroid(lot.geom) AS centroid,
    lot.geom
  FROM
    psi.sales sales
    LEFT JOIN
    psi.lot_property lot
    ON sales.property_id = lot.propid
    WHERE lot.propid IS NOT NULL
    GROUP BY (lot.propid, lot.geom)
);

-- fill in missing geometries where property id didn't match based on the address
-- UPDATE psi.sales_geometry AS sales_geometry SET geom = wkb_geometry
-- FROM psi.property
-- WHERE property.address = format_address(property_unit_number, property_house_number, property_street_name, property_locality);

ANALYZE psi.sales_geometry;
CREATE INDEX IF NOT EXISTS sales_geometry_geom_idx ON psi.sales_geometry USING GIST (geom);
CREATE INDEX IF NOT EXISTS sales_geometry_centroid_idx ON psi.sales_geometry USING GIST (centroid);
CREATE INDEX IF NOT EXISTS sales_geometry_area_idx ON psi.sales_geometry (area);
CREATE INDEX IF NOT EXISTS sales_geometry_contract_date_idx ON psi.sales_geometry (contract_date);
CREATE INDEX IF NOT EXISTS sales_geometry_settlement_date_idx ON psi.sales_geometry (settlement_date);
CREATE INDEX IF NOT EXISTS sales_geometry_purchase_price_idx ON psi.sales_geometry (purchase_price);
