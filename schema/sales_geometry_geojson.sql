CREATE OR REPLACE VIEW psi.sales_geometry_geojson AS
    SELECT
        (property_unit_number IS NOT NULL)::boolean AS strata,
        date_part('year', contract_date)::smallint AS year,
        date_part('month', contract_date)::smallint AS month,
        date_part('day', contract_date)::smallint AS day,
        purchase_price::bigint AS price,
        centroid AS geom
    FROM
        psi.sales_geometry
    WHERE
        centroid IS NOT NULL
;
