-- http://www.valuergeneral.nsw.gov.au/__data/assets/pdf_file/0014/216401/Archived_Property_Sales_Data_File_Format_1990_to_2001_V2.pdf
CREATE UNLOGGED TABLE psi.sales_archived_raw
(
    "record_type" text,
    "district_code" text,
    "source" text,
    "valuation_number" text,
    "property_id" numeric(10),
    "unit_number" text,
    "house_number" text,
    "street_name" text,
    "suburb_name" text,
    "postcode" text,
    "contract_date" text,
    "purchase_price" numeric(12),
    "land_description" text,
    "area" text,
    "area_type" text,
    "dimensions" text,
    "comp_code" text,
    "zone_code" text,
    "vendor_name" text,
    "purchaser_name" text
);

-- http://www.valuergeneral.nsw.gov.au/__data/assets/pdf_file/0015/216402/Current_Property_Sales_Data_File_Format_2001_to_Current.pdf
CREATE UNLOGGED TABLE psi.sales_current_raw
(
    "record_type" text,
    "district_code" text,
    "property_id" text,
    "sale_counter" text,
    "download_date_time" text,
    "property_name" text,
    "property_unit_number" text,
    "property_house_number" text,
    "property_street_name" text,
    "property_locality" text,
    "property_postcode" text,
    "area" numeric,
    "area_type" text,
    "contract_date" text,
    "settlement_date" text,
    "purchase_price" bigint,
    "zoning" text,
    "nature_of_property" text,
    "primary_purpose" text,
    "strata_lot_number" smallint,
    "component_code" text,
    "sale_code" text,
    "percent_of_interest_sale" text,
    "dealing_number" text
);
