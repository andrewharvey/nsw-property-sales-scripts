DROP TABLE IF EXISTS psi.values;
CREATE UNLOGGED TABLE psi.values
(
    "property_id",
    "property_name",
    "property_unit_number",
    "property_house_number",
    "property_street_name",
    "property_locality",
    "property_postcode",
    "property_description",
    "zone_code",
    "area",
    "area_type",
    "valuation_date",
    "land_value"
) AS
SELECT
    "property_id"::integer AS property_id,
    property_name,
    property_unit_number,
    property_house_number,
    property_street_name,
    property_locality,
    property_postcode,
    property_description,
    "zone_code" AS zone,
    "area"::numeric AS area,
    CASE
        WHEN "area_type" = 'M' THEN 'square meters'
        WHEN "area_type" = 'H' THEN 'hectares'
        ELSE NULL
    END AS area_type,
    to_date("base_date_1", 'DD/MM/YYYY') AS valuation_date,
    "land_value_1" AS land_value
FROM psi.values_raw
;

ANALYZE psi.values;
