CREATE INDEX IF NOT EXISTS values_property_id_idx ON psi.values (property_id);

-- CREATE TABLE IF NOT EXISTS psi.values_geometry (LIKE psi.values INCLUDING ALL, geom geometry);

-- assign property_id to lot
DROP TABLE IF EXISTS psi.lot_property;
CREATE TABLE psi.lot_property AS (
  SELECT
    lot.*,
    property.propid
  FROM
    psi.lot lot
    LEFT JOIN
    psi.property property
    ON lot.cadid::integer = property.cadid
);
CREATE INDEX IF NOT EXISTS lot_property_propid_idx ON psi.lot_property (propid);

-- values table + parcel geometry + centroid
DROP TABLE IF EXISTS psi.values_geometry;
CREATE TABLE psi.values_geometry (LIKE psi.values INCLUDING ALL, centroid geometry, geom geometry);
INSERT INTO psi.values_geometry
    SELECT
        --ROW_NUMBER() OVER (ORDER BY 1) AS id,
        values.*,
        ST_Centroid(lot.geom) AS centroid,
        lot.geom AS geom
    FROM
        psi.values values
        LEFT JOIN
        psi.lot_property lot
        ON values.property_id = lot.propid AND lot.propid IS NOT NULL
        ;

ANALYZE psi.values_geometry;
CREATE INDEX IF NOT EXISTS values_geometry_geom_idx ON psi.values_geometry USING GIST (geom);
CREATE INDEX IF NOT EXISTS values_geometry_centroid_idx ON psi.values_geometry USING GIST (centroid);
CREATE INDEX IF NOT EXISTS values_geometry_area_idx ON psi.values_geometry (area);
CREATE INDEX IF NOT EXISTS values_geometry_land_value_idx ON psi.values_geometry (land_value);
