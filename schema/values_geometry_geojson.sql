CREATE OR REPLACE VIEW psi.values_geometry_geojson AS
    SELECT
        (property_unit_number IS NOT NULL)::boolean AS strata,
        zone_code,
        land_value::bigint AS price,
        geom
    FROM
        psi.values_geometry
    WHERE
        geom IS NOT NULL
;
