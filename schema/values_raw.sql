CREATE UNLOGGED TABLE psi.values_raw
(
    "property_id" text,
    "property_type" text,
    "property_name" text,
    "property_unit_number" text,
    "property_house_number" text,
    "property_street_name" text,
    "property_locality" text,
    "property_postcode" text,
    "property_description" text,
    "zone_code" text,
    "area" numeric,
    "area_type" text,
    "base_date_1" text,
    "land_value_1" bigint
);
