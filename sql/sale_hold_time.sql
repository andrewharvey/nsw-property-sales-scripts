-- calculate median hold time of properties

DROP TABLE IF EXISTS psi.average_hold_time;
CREATE TABLE psi.average_hold_time AS (
  SELECT
    property_id,
    address,
    avg((EXTRACT(epoch from hold_time) / 86400)::int) / 365 AS avg_hold_years,
    count(*) AS times_sold,
    centroid
  FROM
    psi.sales_with_purchase
  GROUP BY
    property_id, address, centroid
);

CREATE INDEX IF NOT EXISTS average_hold_time_centroid_idx ON psi.average_hold_time USING GIST (centroid);
