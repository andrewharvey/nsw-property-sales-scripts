-- for each sale, add an additional field for profit it sold for

-- sales table with sale number field
-- where 1 is the first time it sold, incrementing for each sale
DROP TABLE IF EXISTS psi.sales_numbered;
CREATE TABLE psi.sales_numbered AS (
  SELECT
    sales.*,
    ROW_NUMBER() OVER (PARTITION BY property_id, property_locality ORDER BY contract_date) AS sale_number
  FROM
    psi.sales_geometry sales
  WHERE
    purchase_price IS NOT NULL AND purchase_price > 0
);

CREATE INDEX ON psi.sales_numbered (property_id);
CREATE INDEX ON psi.sales_numbered (property_locality);
CREATE INDEX ON psi.sales_numbered (sale_number);

-- for each sale, find the previous sale and caculate profit and hold time
DROP TABLE IF EXISTS psi.sales_with_purchase;
CREATE TABLE psi.sales_with_purchase AS (
  SELECT
    sale.property_id,
    sale.sale_number,
    sale.property_name,
    concat_ws(' ',
      concat_ws('/', sale.property_unit_number, sale.property_house_number),
      sale.property_street_name,
      sale.property_locality
    ) AS address,
    sale.area area,
    sale.contract_date sold_at,
    previous_sale.contract_date purchased_at,
    sale.purchase_price sold_for,
    previous_sale.purchase_price purchased_for,
    AGE(sale.contract_date, previous_sale.contract_date) hold_time,
    sale.purchase_price - previous_sale.purchase_price profit,

    (
      (LEAST(previous_sale.purchase_price, 14000) * 0.0125) +
      ((GREATEST(LEAST(previous_sale.purchase_price, 30000), 14001) - 14001) * 0.024) +
      ((GREATEST(LEAST(previous_sale.purchase_price, 80000), 30001) - 30001) * 0.0175) +
      ((GREATEST(LEAST(previous_sale.purchase_price, 300000), 80001) - 80001) * 0.035) +
      ((GREATEST(LEAST(previous_sale.purchase_price, 1000000), 300001) - 300001) * 0.045) +
      ((GREATEST(LEAST(previous_sale.purchase_price, 3000000), 1000001) - 1000001) * 0.055) +
      ((GREATEST(previous_sale.purchase_price, 3000001) - 3000001) * 0.07)
    )::bigint AS stamp_duty_at_purchase,

    (sale.purchase_price * 0.025)::bigint AS estimated_selling_cost,

    sale.centroid,
    sale.geom
  FROM
    psi.sales_numbered sale
    INNER JOIN
    psi.sales_numbered previous_sale
    ON
      sale.property_id = previous_sale.property_id
      AND
      sale.property_locality = previous_sale.property_locality
      AND
      previous_sale.sale_number = sale.sale_number - 1
);
CREATE INDEX IF NOT EXISTS sales_with_purchase_centroid_idx ON psi.sales_with_purchase USING GIST (centroid);
CREATE INDEX IF NOT EXISTS sales_with_purchase_geom_idx ON psi.sales_with_purchase USING GIST (geom);

--
DROP TABLE IF EXISTS psi.sales_profit_and_loss;
CREATE TABLE psi.sales_profit_and_loss AS (
  SELECT
    sale.*,
    sale.profit - sale.stamp_duty_at_purchase - sale.estimated_selling_cost AS profit_after_costs,

    CASE DATE_PART('day', hold_time)
      WHEN 0 THEN
        NULL
      ELSE
        POWER(((sold_for - estimated_selling_cost) / (purchased_for + stamp_duty_at_purchase))::real, 1 / (
          DATE_PART('year', hold_time) +
          (DATE_PART('month', hold_time) / 12) +
          (DATE_PART('day', hold_time) / 365)
        ))::real - 1
    END AS annual_return
  FROM
    psi.sales_with_purchase AS sale
);
